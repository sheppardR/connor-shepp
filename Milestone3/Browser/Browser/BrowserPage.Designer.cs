﻿using System.Windows.Forms;

namespace Browser
{
    partial class BrowserPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.agletBaseDataSet = new Browser.AgletBaseDataSet();
            this.productsTableAdapter = new Browser.AgletBaseDataSetTableAdapters.ProductsTableAdapter();
            this.txtB_Description = new System.Windows.Forms.TextBox();
            this.txtB_Price = new System.Windows.Forms.TextBox();
            this.txtB_Name = new System.Windows.Forms.TextBox();
            this.comB_product = new System.Windows.Forms.ComboBox();
            this.btn_Display = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agletBaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.agletBaseDataSet;
            // 
            // agletBaseDataSet
            // 
            this.agletBaseDataSet.DataSetName = "AgletBaseDataSet";
            this.agletBaseDataSet.EnforceConstraints = false;
            this.agletBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // txtB_Description
            // 
            this.txtB_Description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.txtB_Description.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Description.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Description.Location = new System.Drawing.Point(215, 231);
            this.txtB_Description.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Description.Multiline = true;
            this.txtB_Description.Name = "txtB_Description";
            this.txtB_Description.ReadOnly = true;
            this.txtB_Description.Size = new System.Drawing.Size(602, 138);
            this.txtB_Description.TabIndex = 19;
            // 
            // txtB_Price
            // 
            this.txtB_Price.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.txtB_Price.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Price.Location = new System.Drawing.Point(215, 188);
            this.txtB_Price.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Price.Name = "txtB_Price";
            this.txtB_Price.ReadOnly = true;
            this.txtB_Price.Size = new System.Drawing.Size(602, 23);
            this.txtB_Price.TabIndex = 17;
            // 
            // txtB_Name
            // 
            this.txtB_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.txtB_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Name.Location = new System.Drawing.Point(215, 145);
            this.txtB_Name.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Name.Name = "txtB_Name";
            this.txtB_Name.ReadOnly = true;
            this.txtB_Name.Size = new System.Drawing.Size(602, 23);
            this.txtB_Name.TabIndex = 14;
            // 
            // comB_product
            // 
            this.comB_product.DataSource = this.productsBindingSource;
            this.comB_product.DisplayMember = "ProductName";
            this.comB_product.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comB_product.FormattingEnabled = true;
            this.comB_product.Location = new System.Drawing.Point(215, 79);
            this.comB_product.Name = "comB_product";
            this.comB_product.Size = new System.Drawing.Size(602, 28);
            this.comB_product.TabIndex = 13;
            this.comB_product.ValueMember = "Id";
            // 
            // btn_Display
            // 
            this.btn_Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Display.Location = new System.Drawing.Point(857, 79);
            this.btn_Display.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Display.Name = "btn_Display";
            this.btn_Display.Size = new System.Drawing.Size(120, 33);
            this.btn_Display.TabIndex = 21;
            this.btn_Display.Text = "Display";
            this.btn_Display.UseVisualStyleBackColor = true;
            this.btn_Display.Click += new System.EventHandler(this.btn_Display_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(211, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 15, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Chose an item to view:";
            // 
            // BrowserPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1032, 654);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Display);
            this.Controls.Add(this.txtB_Description);
            this.Controls.Add(this.txtB_Price);
            this.Controls.Add(this.txtB_Name);
            this.Controls.Add(this.comB_product);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "BrowserPage";
            this.Padding = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Text = "BrowserPage";
            this.Load += new System.EventHandler(this.BrowserPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agletBaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private AgletBaseDataSet agletBaseDataSet;
        private BindingSource productsBindingSource;
        private AgletBaseDataSetTableAdapters.ProductsTableAdapter productsTableAdapter;
        private TextBox txtB_Description;
        private TextBox txtB_Price;
        private TextBox txtB_Name;
        private ComboBox comB_product;
        private Button btn_Display;
        private Label label1;
    }
}

