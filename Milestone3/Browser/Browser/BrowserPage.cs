﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Browser
{
    public partial class BrowserPage : Form
    {
        public BrowserPage()
        {
            InitializeComponent();
        }

        private const string Connect =
            @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        //private const string Connect =
        //@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\Connor-Shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";


        private void BrowserPage_Load(object sender, System.EventArgs e)
        {
            // TODO: This line of code loads data into the 'agletBaseDataSet.Products' table. You can move, or remove it, as needed.
            this.productsTableAdapter.Fill(this.agletBaseDataSet.Products);

        }
        
        private void btn_Display_Click(object sender, System.EventArgs e)
        {
            string result = "";
            var product = comB_product.Text;
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = "SELECT * FROM Products WHERE ProductName = \'" + product + "\'";
                    var command = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    var q = command.ExecuteReader();
                    while (q.Read())
                    {
                        txtB_Name.Text = "Name: " + q["ProductName"];
                        txtB_Price.Text = "Price: $" + q["ProductPrice"];
                        txtB_Description.Text = "Description: " + q["ProductDescription"];
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ERROR - connection in UserMain");
                }
            }
        }
    }
}
