﻿namespace ItemEditor
{
    partial class ItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtB_ProductName = new System.Windows.Forms.TextBox();
            this.txtB_ProductPrice = new System.Windows.Forms.TextBox();
            this.txtB_ProductDescription = new System.Windows.Forms.TextBox();
            this.txtB_Date = new System.Windows.Forms.TextBox();
            this.btn_SaveProduct = new System.Windows.Forms.Button();
            this.error_PriceInvalid = new System.Windows.Forms.Label();
            this.error_NameExists = new System.Windows.Forms.Label();
            this.clearAll = new System.Windows.Forms.Button();
            this.error_NameEmpty = new System.Windows.Forms.Label();
            this.error_PriceEmpty = new System.Windows.Forms.Label();
            this.txtB_Quantity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.error_Quantity = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(15, 8, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Product Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 321);
            this.label2.Margin = new System.Windows.Forms.Padding(15, 8, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(39, 168);
            this.label3.Margin = new System.Windows.Forms.Padding(15, 8, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 387);
            this.label4.Margin = new System.Windows.Forms.Padding(15, 8, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Update Date (e.g. 07/22/96)";
            // 
            // txtB_ProductName
            // 
            this.txtB_ProductName.Location = new System.Drawing.Point(33, 60);
            this.txtB_ProductName.Name = "txtB_ProductName";
            this.txtB_ProductName.Size = new System.Drawing.Size(424, 23);
            this.txtB_ProductName.TabIndex = 5;
            // 
            // txtB_ProductPrice
            // 
            this.txtB_ProductPrice.Location = new System.Drawing.Point(33, 349);
            this.txtB_ProductPrice.Name = "txtB_ProductPrice";
            this.txtB_ProductPrice.Size = new System.Drawing.Size(424, 23);
            this.txtB_ProductPrice.TabIndex = 6;
            // 
            // txtB_ProductDescription
            // 
            this.txtB_ProductDescription.Location = new System.Drawing.Point(33, 196);
            this.txtB_ProductDescription.Multiline = true;
            this.txtB_ProductDescription.Name = "txtB_ProductDescription";
            this.txtB_ProductDescription.Size = new System.Drawing.Size(424, 70);
            this.txtB_ProductDescription.TabIndex = 7;
            // 
            // txtB_Date
            // 
            this.txtB_Date.Location = new System.Drawing.Point(33, 415);
            this.txtB_Date.Multiline = true;
            this.txtB_Date.Name = "txtB_Date";
            this.txtB_Date.Size = new System.Drawing.Size(424, 35);
            this.txtB_Date.TabIndex = 8;
            // 
            // btn_SaveProduct
            // 
            this.btn_SaveProduct.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SaveProduct.Location = new System.Drawing.Point(86, 483);
            this.btn_SaveProduct.Name = "btn_SaveProduct";
            this.btn_SaveProduct.Size = new System.Drawing.Size(120, 35);
            this.btn_SaveProduct.TabIndex = 9;
            this.btn_SaveProduct.Text = "Save";
            this.btn_SaveProduct.UseVisualStyleBackColor = true;
            this.btn_SaveProduct.Click += new System.EventHandler(this.btn_SaveProduct_Click);
            // 
            // error_PriceInvalid
            // 
            this.error_PriceInvalid.AutoSize = true;
            this.error_PriceInvalid.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_PriceInvalid.ForeColor = System.Drawing.Color.Red;
            this.error_PriceInvalid.Location = new System.Drawing.Point(214, 321);
            this.error_PriceInvalid.Name = "error_PriceInvalid";
            this.error_PriceInvalid.Size = new System.Drawing.Size(191, 20);
            this.error_PriceInvalid.TabIndex = 11;
            this.error_PriceInvalid.Text = "Please enter a valid number";
            this.error_PriceInvalid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.error_PriceInvalid.Visible = false;
            // 
            // error_NameExists
            // 
            this.error_NameExists.AutoSize = true;
            this.error_NameExists.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_NameExists.ForeColor = System.Drawing.Color.Red;
            this.error_NameExists.Location = new System.Drawing.Point(250, 33);
            this.error_NameExists.Name = "error_NameExists";
            this.error_NameExists.Size = new System.Drawing.Size(161, 20);
            this.error_NameExists.TabIndex = 12;
            this.error_NameExists.Text = "This item already exists";
            this.error_NameExists.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.error_NameExists.Visible = false;
            // 
            // clearAll
            // 
            this.clearAll.Location = new System.Drawing.Point(271, 482);
            this.clearAll.Name = "clearAll";
            this.clearAll.Size = new System.Drawing.Size(120, 36);
            this.clearAll.TabIndex = 13;
            this.clearAll.Text = "Clear All";
            this.clearAll.UseVisualStyleBackColor = true;
            this.clearAll.Click += new System.EventHandler(this.clearAll_Click);
            // 
            // error_NameEmpty
            // 
            this.error_NameEmpty.AutoSize = true;
            this.error_NameEmpty.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_NameEmpty.ForeColor = System.Drawing.Color.Red;
            this.error_NameEmpty.Location = new System.Drawing.Point(267, 33);
            this.error_NameEmpty.Name = "error_NameEmpty";
            this.error_NameEmpty.Size = new System.Drawing.Size(149, 20);
            this.error_NameEmpty.TabIndex = 14;
            this.error_NameEmpty.Text = "Item must have name";
            this.error_NameEmpty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.error_NameEmpty.Visible = false;
            // 
            // error_PriceEmpty
            // 
            this.error_PriceEmpty.AutoSize = true;
            this.error_PriceEmpty.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_PriceEmpty.ForeColor = System.Drawing.Color.Red;
            this.error_PriceEmpty.Location = new System.Drawing.Point(296, 321);
            this.error_PriceEmpty.Name = "error_PriceEmpty";
            this.error_PriceEmpty.Size = new System.Drawing.Size(115, 20);
            this.error_PriceEmpty.TabIndex = 15;
            this.error_PriceEmpty.Text = "Must enter price";
            this.error_PriceEmpty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.error_PriceEmpty.Visible = false;
            // 
            // txtB_Quantity
            // 
            this.txtB_Quantity.Location = new System.Drawing.Point(33, 128);
            this.txtB_Quantity.Name = "txtB_Quantity";
            this.txtB_Quantity.Size = new System.Drawing.Size(424, 23);
            this.txtB_Quantity.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 100);
            this.label5.Margin = new System.Windows.Forms.Padding(15, 8, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Quantity";
            // 
            // error_Quantity
            // 
            this.error_Quantity.AutoSize = true;
            this.error_Quantity.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_Quantity.ForeColor = System.Drawing.Color.Red;
            this.error_Quantity.Location = new System.Drawing.Point(296, 105);
            this.error_Quantity.Name = "error_Quantity";
            this.error_Quantity.Size = new System.Drawing.Size(135, 20);
            this.error_Quantity.TabIndex = 18;
            this.error_Quantity.Text = "Must enter quantity";
            this.error_Quantity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.error_Quantity.Visible = false;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(512, 321);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 19;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // ItemEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 551);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.error_Quantity);
            this.Controls.Add(this.txtB_Quantity);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.error_PriceEmpty);
            this.Controls.Add(this.error_NameEmpty);
            this.Controls.Add(this.clearAll);
            this.Controls.Add(this.error_NameExists);
            this.Controls.Add(this.error_PriceInvalid);
            this.Controls.Add(this.btn_SaveProduct);
            this.Controls.Add(this.txtB_Date);
            this.Controls.Add(this.txtB_ProductDescription);
            this.Controls.Add(this.txtB_ProductPrice);
            this.Controls.Add(this.txtB_ProductName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ItemEditor";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Edit Product";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtB_ProductName;
        private System.Windows.Forms.TextBox txtB_ProductPrice;
        private System.Windows.Forms.TextBox txtB_ProductDescription;
        private System.Windows.Forms.TextBox txtB_Date;
        private System.Windows.Forms.Button btn_SaveProduct;
        private System.Windows.Forms.Label error_PriceInvalid;
        private System.Windows.Forms.Label error_NameExists;
        private System.Windows.Forms.Button clearAll;
        private System.Windows.Forms.Label error_NameEmpty;
        private System.Windows.Forms.Label error_PriceEmpty;
        private System.Windows.Forms.TextBox txtB_Quantity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label error_Quantity;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
    }
}

