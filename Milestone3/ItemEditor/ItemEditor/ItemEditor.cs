﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfirmForm;
using System.Data.SqlClient;

namespace ItemEditor
{
    public partial class ItemEditor : Form
    {
        private const string Connect =
           @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";


        //private const string Connect =
        //  @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\Connor-Shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        public ItemEditor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private String ProductName;
        private double ProductQuantity;
        private String ProductDescription;
        private double ProductPrice;
        private String Date;
        private Boolean Duplicate = false;

        private void btn_SaveProduct_Click(object sender, EventArgs e)
        {
            ProductName = txtB_ProductName.Text;
            ProductQuantity = double.Parse(txtB_Quantity.Text);
            ProductDescription = txtB_ProductDescription.Text;
            ProductPrice = double.Parse(txtB_ProductPrice.Text);
            Date = txtB_Date.Text;

            double price;
            bool priceSuccess;
            bool nameSuccess;

            Check(ProductName);
            // Errors for Price box
            if (txtB_ProductPrice.Text.Equals(string.Empty))
            {
                error_PriceInvalid.Hide();
                error_PriceEmpty.Show();
                priceSuccess = false;
            }
            else if (!double.TryParse(txtB_ProductPrice.Text, out price))
            {
                error_PriceInvalid.Show();
                error_PriceEmpty.Hide();
                priceSuccess = false;
            }
            else
            {
                priceSuccess = true;
                error_PriceInvalid.Hide();
                error_PriceEmpty.Hide();
            }

            // Errors for Name Box
            if (txtB_ProductName.Text.Equals(string.Empty))
            {
                error_NameExists.Hide();
                error_NameEmpty.Show();
                nameSuccess = false;
            }
            else if (Duplicate)
            {
                error_NameExists.Show();
                error_NameEmpty.Hide();
                nameSuccess = false;
            }
            else
            {
                nameSuccess = true;
                error_NameExists.Hide();
                error_NameEmpty.Hide();
            }

            // if forms are filled, display confirmation dialogue
            bool success = priceSuccess && nameSuccess;
            if (!success) return;
            var f = new ConfirmForm.Confirm();
            f.ShowDialog();
            if (Confirm.YesPressed)
            {
                this.Hide();
                AddItem(ProductName, ProductQuantity, ProductDescription, ProductPrice, Date);
                
            }
        }

        private void Check(String product)
        {
            product = ProductName;

            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"SELECT * FROM Products";
                    var comm = new SqlCommand(query, conn);

                    conn.Open();

                    SqlDataReader dr = comm.ExecuteReader();
                    while (dr.Read())
                    {
                        String val = dr["ProductName"] + "";
                        if (val.Equals(product))
                        {
                            Duplicate = true;
                        }
                        else Duplicate = false;



                    }
                    conn.Close();

                }
                catch (Exception)
                {
                    MessageBox.Show("Errrr");
                }
            }
        }

        private void AddItem(String name, double quantity, string desc, double price, string date)
        {
            //ProductName, ProductQuantity, ProductDescription, ProductPrice, ProductSold, UpdateDate
            name = ProductName;
            quantity = ProductQuantity;
            desc = ProductDescription;
            price = ProductPrice;
            date = Date;
            int sold = 0;

            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"INSERT INTO Products (ProductName, ProductQuantity, ProductDescription, ProductPrice, ProductSold, UpdateDate) 
                    VALUES ('" + name + @"', '" + quantity + @"', '" + desc + @"', '" + price + @"', '" + sold + @"', '" + date + @"');";
                    var comm = new SqlCommand(query, conn);
                    //execute query
                    conn.Open();
                    comm.ExecuteReader();

                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ERROR");
                }
            }
        }

        private void clearAll_Click(object sender, EventArgs e)
        {
            txtB_ProductDescription.Text = txtB_ProductName.Text = txtB_ProductPrice.Text = txtB_Date.Text = "";
            error_NameExists.Hide();
            error_PriceInvalid.Hide();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            txtB_Date.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();
        }
    }
}
