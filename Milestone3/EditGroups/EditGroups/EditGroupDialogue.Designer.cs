﻿namespace EditGroups
{
    partial class EditGroupDialogue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstB_Groups = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_RemoveGroup = new System.Windows.Forms.Button();
            this.btn_EditGroup = new System.Windows.Forms.Button();
            this.errorMessage_TitleEmpty = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstB_Groups
            // 
            this.lstB_Groups.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstB_Groups.FormattingEnabled = true;
            this.lstB_Groups.IntegralHeight = false;
            this.lstB_Groups.ItemHeight = 20;
            this.lstB_Groups.Items.AddRange(new object[] {
            "Admin",
            "Clerk",
            "General Manager",
            "Inventory Manager"});
            this.lstB_Groups.Location = new System.Drawing.Point(35, 76);
            this.lstB_Groups.Margin = new System.Windows.Forms.Padding(5);
            this.lstB_Groups.Name = "lstB_Groups";
            this.lstB_Groups.Size = new System.Drawing.Size(225, 228);
            this.lstB_Groups.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(297, 79);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Group Title";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(395, 76);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(204, 23);
            this.textBox1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(297, 123);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Privileges";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(10, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 30);
            this.label3.TabIndex = 7;
            this.label3.Text = "Current Groups";
            // 
            // btn_RemoveGroup
            // 
            this.btn_RemoveGroup.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RemoveGroup.Location = new System.Drawing.Point(35, 322);
            this.btn_RemoveGroup.Name = "btn_RemoveGroup";
            this.btn_RemoveGroup.Size = new System.Drawing.Size(100, 35);
            this.btn_RemoveGroup.TabIndex = 8;
            this.btn_RemoveGroup.Text = "Remove";
            this.btn_RemoveGroup.UseVisualStyleBackColor = true;
            this.btn_RemoveGroup.Click += new System.EventHandler(this.btn_RemoveGroup_Click);
            // 
            // btn_EditGroup
            // 
            this.btn_EditGroup.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditGroup.Location = new System.Drawing.Point(395, 322);
            this.btn_EditGroup.Name = "btn_EditGroup";
            this.btn_EditGroup.Size = new System.Drawing.Size(100, 35);
            this.btn_EditGroup.TabIndex = 9;
            this.btn_EditGroup.Text = "Add New";
            this.btn_EditGroup.UseVisualStyleBackColor = true;
            this.btn_EditGroup.Click += new System.EventHandler(this.btn_EditGroup_Click);
            // 
            // errorMessage_TitleEmpty
            // 
            this.errorMessage_TitleEmpty.AutoSize = true;
            this.errorMessage_TitleEmpty.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_TitleEmpty.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_TitleEmpty.Location = new System.Drawing.Point(492, 107);
            this.errorMessage_TitleEmpty.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_TitleEmpty.Name = "errorMessage_TitleEmpty";
            this.errorMessage_TitleEmpty.Size = new System.Drawing.Size(107, 20);
            this.errorMessage_TitleEmpty.TabIndex = 19;
            this.errorMessage_TitleEmpty.Text = "Must set a title";
            this.errorMessage_TitleEmpty.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(395, 129);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(85, 21);
            this.checkBox1.TabIndex = 20;
            this.checkBox1.Text = "Edit User";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(395, 166);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(102, 21);
            this.checkBox2.TabIndex = 21;
            this.checkBox2.Text = "Edit Groups";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(395, 203);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(111, 21);
            this.checkBox3.TabIndex = 22;
            this.checkBox3.Text = "Edit Products";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(395, 241);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(112, 21);
            this.checkBox4.TabIndex = 23;
            this.checkBox4.Text = "Update Stock";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(395, 277);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(126, 21);
            this.checkBox5.TabIndex = 24;
            this.checkBox5.Text = "Manage Orders";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(296, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 30);
            this.label4.TabIndex = 25;
            this.label4.Text = "Add Group";
            
            // 
            // EditGroupDialogue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 389);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.errorMessage_TitleEmpty);
            this.Controls.Add(this.btn_EditGroup);
            this.Controls.Add(this.btn_RemoveGroup);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstB_Groups);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditGroupDialogue";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Edit Groups";
            this.Load += new System.EventHandler(this.EditGroupDialogue_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstB_Groups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_RemoveGroup;
        private System.Windows.Forms.Button btn_EditGroup;
        private System.Windows.Forms.Label errorMessage_TitleEmpty;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Label label4;
    }
}

