﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfirmForm;
using System.Data.SqlClient;

namespace EditGroups
{
    public partial class EditGroupDialogue : Form
    {
        private const string Connect =
          @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        //private const string Connect =
        //    @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\Connor-Shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        public EditGroupDialogue()
        {
            InitializeComponent();

        }

        private string GroupName;
        private string GroupRemove;
        private Boolean EditUserBox = false;
        private Boolean EditGroupBox = false;
        private Boolean EditProductBox = false;
        private Boolean UpdateStockBox = false;
        private Boolean ManageOrderBox = false;

        private void btn_EditGroup_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(String.Empty))
                errorMessage_TitleEmpty.Show();
            else
            {
                errorMessage_TitleEmpty.Hide();
                var f = new ConfirmForm.Confirm();
                f.ShowDialog();
                if (Confirm.YesPressed)
                {
                    //this.Hide();
                   
                    if(checkBox1.Checked) {
                        EditUserBox = true;
                    }
                    
                    if(checkBox2.Checked)
                    {
                        EditGroupBox = true;
                    }

                    if (checkBox3.Checked)
                    {
                        EditProductBox = true;
                    }

                    if (checkBox4.Checked)
                    {
                        UpdateStockBox = true;
                    }

                    if (checkBox5.Checked)
                    {
                        ManageOrderBox = true;
                    }

                    AddGroup(GroupName, EditUserBox, EditGroupBox, EditProductBox, UpdateStockBox, ManageOrderBox);
                    LoadListBox(lstB_Groups);
                    textBox1.Text = "";
                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;

                }
            }
        }

        private void LoadListBox(ListBox Target)
        {
            
            Target.Items.Clear();
            Target.Text = "";
            using(SqlConnection conn = new SqlConnection(Connect))
            {
                try
                {
                    string query = "SELECT * FROM Groups";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.CommandText = query;
                    conn.Open();
                    SqlDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        String val = dr["GroupName"].ToString();
                        Target.Items.Add(val);
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void AddGroup(String n, Boolean Users, Boolean Groups, Boolean EditProduct, Boolean Stock, Boolean Orders)
        {
           
            n = textBox1.Text;
            Users = EditUserBox;
            Groups = EditGroupBox;
            EditProduct = EditProductBox;
            Stock = UpdateStockBox;
            Orders = ManageOrderBox;
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    // generate query
                    var Query =
                       @"INSERT INTO Groups (GroupName, EditUser, EditGroup, EditProduct, UpdateStock, ManageOrders) 
VALUES ('" + n + @"', '" + Users + @"', '" + Groups + @"', '" + EditProduct + @"', '" + Stock + @"', '" + Orders + @"');";
                    var comm = new SqlCommand(Query, conn);
                    // execute query
                    conn.Open();
                    comm.ExecuteReader();
                    
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ERROR - Connection");
                }
            }
        }

        private void EditGroupDialogue_Load(object sender, EventArgs e)
        {
            LoadListBox(lstB_Groups);
        }

        private void RemoveGroup(String Name)
        {
            Name = GroupRemove;
            using (SqlConnection conn = new SqlConnection(Connect))
            {
                try
                {
                    string query = @"DELETE FROM Groups WHERE GroupName = '" + GroupRemove + @"'";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.CommandText = query;
                    conn.Open();
                    SqlDataReader dr = command.ExecuteReader();
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        

        private void btn_RemoveGroup_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstB_Groups.Text.Length; i++)
            {
                var str = lstB_Groups.Text.Substring(i, 1);
                if (str.Equals(" ") && lstB_Groups.Text.Substring(i + 1, 1).Equals(" "))
                    break;
                GroupRemove += str;
            }

            RemoveGroup(GroupRemove);
            LoadListBox(lstB_Groups);
        }

        
    }
}
