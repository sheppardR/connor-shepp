﻿namespace Milestone3
{
    partial class StaffView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.page_StoUpd = new System.Windows.Forms.TabPage();
            this.block_StockUpdate = new System.Windows.Forms.Panel();
            this.txtB_AddStock = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_AddStock = new System.Windows.Forms.Button();
            this.txtB_UpdateDate = new System.Windows.Forms.TextBox();
            this.txtB_ProductPrice = new System.Windows.Forms.TextBox();
            this.txtB_ProductQuantity = new System.Windows.Forms.TextBox();
            this.txtB_ProductName = new System.Windows.Forms.TextBox();
            this.comB_AddStock = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.products = new Milestone3.Products();
            this.staffBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.staffBaseDataSet2 = new Milestone3.StaffBaseDataSet2();
            this.page_UsePri = new System.Windows.Forms.TabPage();
            this.block_UserPrivileges = new System.Windows.Forms.Panel();
            this.btn_DisplayUser = new System.Windows.Forms.Button();
            this.txtB_Notes = new System.Windows.Forms.TextBox();
            this.txtB_Phone = new System.Windows.Forms.TextBox();
            this.txtB_Position = new System.Windows.Forms.TextBox();
            this.txtB_StaffName = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btn_EditGroups = new System.Windows.Forms.Button();
            this.btn_RemoveUser = new System.Windows.Forms.Button();
            this.btn_Edit = new System.Windows.Forms.Button();
            this.rText_UserInformation = new System.Windows.Forms.RichTextBox();
            this.page_OrdReq = new System.Windows.Forms.TabPage();
            this.block_OrderRequest = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.productError = new System.Windows.Forms.Label();
            this.quantityError = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.btn_RequestSubmit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtB_RequestDate = new System.Windows.Forms.TextBox();
            this.txtB_RequestQuantity = new System.Windows.Forms.TextBox();
            this.txtB_RequestProduct = new System.Windows.Forms.TextBox();
            this.page_ProMan = new System.Windows.Forms.TabPage();
            this.block_ProductManager = new System.Windows.Forms.Panel();
            this.txtB_Id = new System.Windows.Forms.TextBox();
            this.txtB_Description = new System.Windows.Forms.TextBox();
            this.txtB_Update = new System.Windows.Forms.TextBox();
            this.txtB_Price = new System.Windows.Forms.TextBox();
            this.txtB_Sold = new System.Windows.Forms.TextBox();
            this.txtB_Quanity = new System.Windows.Forms.TextBox();
            this.txtB_Name = new System.Windows.Forms.TextBox();
            this.comB_product = new System.Windows.Forms.ComboBox();
            this.btn_Display = new System.Windows.Forms.Button();
            this.btn_RemoveProduct = new System.Windows.Forms.Button();
            this.btn_AddProduct = new System.Windows.Forms.Button();
            this.tabI = new System.Windows.Forms.TabControl();
            this.productsTableAdapter = new Milestone3.ProductsTableAdapters.ProductsTableAdapter();
            this.staffTableAdapter = new Milestone3.StaffBaseDataSet2TableAdapters.StaffTableAdapter();
            this.page_StoUpd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.products)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBaseDataSet2)).BeginInit();
            this.page_UsePri.SuspendLayout();
            this.page_OrdReq.SuspendLayout();
            this.panel1.SuspendLayout();
            this.page_ProMan.SuspendLayout();
            this.tabI.SuspendLayout();
            this.SuspendLayout();
            // 
            // page_StoUpd
            // 
            this.page_StoUpd.Controls.Add(this.block_StockUpdate);
            this.page_StoUpd.Controls.Add(this.txtB_AddStock);
            this.page_StoUpd.Controls.Add(this.button1);
            this.page_StoUpd.Controls.Add(this.btn_AddStock);
            this.page_StoUpd.Controls.Add(this.txtB_UpdateDate);
            this.page_StoUpd.Controls.Add(this.txtB_ProductPrice);
            this.page_StoUpd.Controls.Add(this.txtB_ProductQuantity);
            this.page_StoUpd.Controls.Add(this.txtB_ProductName);
            this.page_StoUpd.Controls.Add(this.comB_AddStock);
            this.page_StoUpd.Controls.Add(this.richTextBox1);
            this.page_StoUpd.Location = new System.Drawing.Point(4, 54);
            this.page_StoUpd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_StoUpd.Name = "page_StoUpd";
            this.page_StoUpd.Size = new System.Drawing.Size(1024, 596);
            this.page_StoUpd.TabIndex = 5;
            this.page_StoUpd.Text = "Stock Update";
            this.page_StoUpd.UseVisualStyleBackColor = true;
            // 
            // block_StockUpdate
            // 
            this.block_StockUpdate.Location = new System.Drawing.Point(8, 9);
            this.block_StockUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.block_StockUpdate.Name = "block_StockUpdate";
            this.block_StockUpdate.Size = new System.Drawing.Size(1008, 580);
            this.block_StockUpdate.TabIndex = 23;
            // 
            // txtB_AddStock
            // 
            this.txtB_AddStock.Location = new System.Drawing.Point(156, 468);
            this.txtB_AddStock.Margin = new System.Windows.Forms.Padding(4);
            this.txtB_AddStock.Name = "txtB_AddStock";
            this.txtB_AddStock.Size = new System.Drawing.Size(145, 30);
            this.txtB_AddStock.TabIndex = 32;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(829, 25);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 33);
            this.button1.TabIndex = 31;
            this.button1.Text = "Display";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_AddStock
            // 
            this.btn_AddStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(224)))), ((int)(((byte)(222)))));
            this.btn_AddStock.FlatAppearance.BorderSize = 0;
            this.btn_AddStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AddStock.Location = new System.Drawing.Point(317, 466);
            this.btn_AddStock.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_AddStock.Name = "btn_AddStock";
            this.btn_AddStock.Size = new System.Drawing.Size(120, 34);
            this.btn_AddStock.TabIndex = 30;
            this.btn_AddStock.Text = "Add Stock";
            this.btn_AddStock.UseVisualStyleBackColor = false;
            this.btn_AddStock.Click += new System.EventHandler(this.btn_AddStock_Click);
            // 
            // txtB_UpdateDate
            // 
            this.txtB_UpdateDate.BackColor = System.Drawing.Color.White;
            this.txtB_UpdateDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_UpdateDate.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_UpdateDate.Location = new System.Drawing.Point(156, 309);
            this.txtB_UpdateDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_UpdateDate.Multiline = true;
            this.txtB_UpdateDate.Name = "txtB_UpdateDate";
            this.txtB_UpdateDate.ReadOnly = true;
            this.txtB_UpdateDate.Size = new System.Drawing.Size(603, 138);
            this.txtB_UpdateDate.TabIndex = 29;
            this.txtB_UpdateDate.Text = "Update Date:";
            // 
            // txtB_ProductPrice
            // 
            this.txtB_ProductPrice.BackColor = System.Drawing.Color.White;
            this.txtB_ProductPrice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_ProductPrice.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_ProductPrice.Location = new System.Drawing.Point(156, 236);
            this.txtB_ProductPrice.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_ProductPrice.Name = "txtB_ProductPrice";
            this.txtB_ProductPrice.ReadOnly = true;
            this.txtB_ProductPrice.Size = new System.Drawing.Size(603, 30);
            this.txtB_ProductPrice.TabIndex = 28;
            this.txtB_ProductPrice.Text = "Price:";
            // 
            // txtB_ProductQuantity
            // 
            this.txtB_ProductQuantity.BackColor = System.Drawing.Color.White;
            this.txtB_ProductQuantity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_ProductQuantity.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_ProductQuantity.Location = new System.Drawing.Point(156, 166);
            this.txtB_ProductQuantity.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_ProductQuantity.Name = "txtB_ProductQuantity";
            this.txtB_ProductQuantity.ReadOnly = true;
            this.txtB_ProductQuantity.Size = new System.Drawing.Size(603, 30);
            this.txtB_ProductQuantity.TabIndex = 27;
            this.txtB_ProductQuantity.Text = "Quantity:";
            // 
            // txtB_ProductName
            // 
            this.txtB_ProductName.BackColor = System.Drawing.Color.White;
            this.txtB_ProductName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_ProductName.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_ProductName.Location = new System.Drawing.Point(156, 90);
            this.txtB_ProductName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_ProductName.Name = "txtB_ProductName";
            this.txtB_ProductName.ReadOnly = true;
            this.txtB_ProductName.Size = new System.Drawing.Size(603, 30);
            this.txtB_ProductName.TabIndex = 26;
            this.txtB_ProductName.Text = "Name:";
            // 
            // comB_AddStock
            // 
            this.comB_AddStock.FormattingEnabled = true;
            this.comB_AddStock.Location = new System.Drawing.Point(156, 25);
            this.comB_AddStock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comB_AddStock.Name = "comB_AddStock";
            this.comB_AddStock.Size = new System.Drawing.Size(601, 33);
            this.comB_AddStock.TabIndex = 25;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(137, 82);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(15, 10, 15, 2);
            this.richTextBox1.MaxLength = 500;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox1.Size = new System.Drawing.Size(637, 474);
            this.richTextBox1.TabIndex = 24;
            this.richTextBox1.Text = "";
            this.richTextBox1.UseWaitCursor = true;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.products;
            // 
            // products
            // 
            this.products.DataSetName = "Products";
            this.products.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // staffBindingSource
            // 
            this.staffBindingSource.DataMember = "Staff";
            this.staffBindingSource.DataSource = this.staffBaseDataSet2;
            // 
            // staffBaseDataSet2
            // 
            this.staffBaseDataSet2.DataSetName = "StaffBaseDataSet2";
            this.staffBaseDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // page_UsePri
            // 
            this.page_UsePri.Controls.Add(this.block_UserPrivileges);
            this.page_UsePri.Controls.Add(this.btn_DisplayUser);
            this.page_UsePri.Controls.Add(this.txtB_Notes);
            this.page_UsePri.Controls.Add(this.txtB_Phone);
            this.page_UsePri.Controls.Add(this.txtB_Position);
            this.page_UsePri.Controls.Add(this.txtB_StaffName);
            this.page_UsePri.Controls.Add(this.comboBox1);
            this.page_UsePri.Controls.Add(this.btn_EditGroups);
            this.page_UsePri.Controls.Add(this.btn_RemoveUser);
            this.page_UsePri.Controls.Add(this.btn_Edit);
            this.page_UsePri.Controls.Add(this.rText_UserInformation);
            this.page_UsePri.Location = new System.Drawing.Point(4, 54);
            this.page_UsePri.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_UsePri.Name = "page_UsePri";
            this.page_UsePri.Size = new System.Drawing.Size(1024, 596);
            this.page_UsePri.TabIndex = 4;
            this.page_UsePri.Text = "User Privileges";
            this.page_UsePri.UseVisualStyleBackColor = true;
            // 
            // block_UserPrivileges
            // 
            this.block_UserPrivileges.Location = new System.Drawing.Point(8, 9);
            this.block_UserPrivileges.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.block_UserPrivileges.Name = "block_UserPrivileges";
            this.block_UserPrivileges.Size = new System.Drawing.Size(1008, 580);
            this.block_UserPrivileges.TabIndex = 22;
            // 
            // btn_DisplayUser
            // 
            this.btn_DisplayUser.Location = new System.Drawing.Point(843, 31);
            this.btn_DisplayUser.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DisplayUser.Name = "btn_DisplayUser";
            this.btn_DisplayUser.Size = new System.Drawing.Size(120, 33);
            this.btn_DisplayUser.TabIndex = 21;
            this.btn_DisplayUser.Text = "Display";
            this.btn_DisplayUser.UseVisualStyleBackColor = true;
            this.btn_DisplayUser.Click += new System.EventHandler(this.btn_DisplayUser_Click);
            // 
            // txtB_Notes
            // 
            this.txtB_Notes.BackColor = System.Drawing.Color.White;
            this.txtB_Notes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Notes.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Notes.Location = new System.Drawing.Point(209, 315);
            this.txtB_Notes.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Notes.Multiline = true;
            this.txtB_Notes.Name = "txtB_Notes";
            this.txtB_Notes.ReadOnly = true;
            this.txtB_Notes.Size = new System.Drawing.Size(603, 138);
            this.txtB_Notes.TabIndex = 19;
            this.txtB_Notes.Text = "Notes:";
            // 
            // txtB_Phone
            // 
            this.txtB_Phone.BackColor = System.Drawing.Color.White;
            this.txtB_Phone.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Phone.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Phone.Location = new System.Drawing.Point(209, 242);
            this.txtB_Phone.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Phone.Name = "txtB_Phone";
            this.txtB_Phone.ReadOnly = true;
            this.txtB_Phone.Size = new System.Drawing.Size(603, 30);
            this.txtB_Phone.TabIndex = 16;
            this.txtB_Phone.Text = "Phone Number:";
            // 
            // txtB_Position
            // 
            this.txtB_Position.BackColor = System.Drawing.Color.White;
            this.txtB_Position.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Position.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_Position.Location = new System.Drawing.Point(209, 172);
            this.txtB_Position.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Position.Name = "txtB_Position";
            this.txtB_Position.ReadOnly = true;
            this.txtB_Position.Size = new System.Drawing.Size(603, 30);
            this.txtB_Position.TabIndex = 15;
            this.txtB_Position.Text = "Position:";
            // 
            // txtB_StaffName
            // 
            this.txtB_StaffName.BackColor = System.Drawing.Color.White;
            this.txtB_StaffName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_StaffName.Font = new System.Drawing.Font("Franklin Gothic Medium", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_StaffName.Location = new System.Drawing.Point(209, 96);
            this.txtB_StaffName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_StaffName.Name = "txtB_StaffName";
            this.txtB_StaffName.ReadOnly = true;
            this.txtB_StaffName.Size = new System.Drawing.Size(603, 30);
            this.txtB_StaffName.TabIndex = 14;
            this.txtB_StaffName.Text = "Name:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(209, 31);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(601, 33);
            this.comboBox1.TabIndex = 13;
            // 
            // btn_EditGroups
            // 
            this.btn_EditGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(224)))), ((int)(((byte)(222)))));
            this.btn_EditGroups.FlatAppearance.BorderSize = 0;
            this.btn_EditGroups.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_EditGroups.Location = new System.Drawing.Point(843, 315);
            this.btn_EditGroups.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_EditGroups.Name = "btn_EditGroups";
            this.btn_EditGroups.Size = new System.Drawing.Size(120, 34);
            this.btn_EditGroups.TabIndex = 9;
            this.btn_EditGroups.Text = "Groups";
            this.btn_EditGroups.UseVisualStyleBackColor = false;
            this.btn_EditGroups.Click += new System.EventHandler(this.btn_EditGroups_Click);
            // 
            // btn_RemoveUser
            // 
            this.btn_RemoveUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_RemoveUser.FlatAppearance.BorderSize = 0;
            this.btn_RemoveUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RemoveUser.Location = new System.Drawing.Point(843, 250);
            this.btn_RemoveUser.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_RemoveUser.Name = "btn_RemoveUser";
            this.btn_RemoveUser.Size = new System.Drawing.Size(120, 34);
            this.btn_RemoveUser.TabIndex = 7;
            this.btn_RemoveUser.Text = "Remove";
            this.btn_RemoveUser.UseVisualStyleBackColor = false;
            // 
            // btn_Edit
            // 
            this.btn_Edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(224)))), ((int)(((byte)(222)))));
            this.btn_Edit.FlatAppearance.BorderSize = 0;
            this.btn_Edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Edit.Location = new System.Drawing.Point(843, 183);
            this.btn_Edit.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(120, 34);
            this.btn_Edit.TabIndex = 6;
            this.btn_Edit.Text = "Add User";
            this.btn_Edit.UseVisualStyleBackColor = false;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // rText_UserInformation
            // 
            this.rText_UserInformation.BackColor = System.Drawing.SystemColors.Window;
            this.rText_UserInformation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rText_UserInformation.Location = new System.Drawing.Point(191, 89);
            this.rText_UserInformation.Margin = new System.Windows.Forms.Padding(15, 10, 15, 2);
            this.rText_UserInformation.MaxLength = 500;
            this.rText_UserInformation.Name = "rText_UserInformation";
            this.rText_UserInformation.ReadOnly = true;
            this.rText_UserInformation.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rText_UserInformation.Size = new System.Drawing.Size(637, 474);
            this.rText_UserInformation.TabIndex = 5;
            this.rText_UserInformation.Text = "";
            this.rText_UserInformation.UseWaitCursor = true;
            // 
            // page_OrdReq
            // 
            this.page_OrdReq.Controls.Add(this.block_OrderRequest);
            this.page_OrdReq.Controls.Add(this.panel1);
            this.page_OrdReq.Location = new System.Drawing.Point(4, 54);
            this.page_OrdReq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_OrdReq.Name = "page_OrdReq";
            this.page_OrdReq.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_OrdReq.Size = new System.Drawing.Size(1024, 596);
            this.page_OrdReq.TabIndex = 1;
            this.page_OrdReq.Text = "Order Request";
            this.page_OrdReq.UseVisualStyleBackColor = true;
            // 
            // block_OrderRequest
            // 
            this.block_OrderRequest.Location = new System.Drawing.Point(8, 7);
            this.block_OrderRequest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.block_OrderRequest.Name = "block_OrderRequest";
            this.block_OrderRequest.Size = new System.Drawing.Size(1008, 580);
            this.block_OrderRequest.TabIndex = 23;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.productError);
            this.panel1.Controls.Add(this.quantityError);
            this.panel1.Controls.Add(this.monthCalendar1);
            this.panel1.Controls.Add(this.btn_RequestSubmit);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtB_RequestDate);
            this.panel1.Controls.Add(this.txtB_RequestQuantity);
            this.panel1.Controls.Add(this.txtB_RequestProduct);
            this.panel1.Location = new System.Drawing.Point(261, 22);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(599, 526);
            this.panel1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(320, 473);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 34);
            this.button2.TabIndex = 10;
            this.button2.Text = "Clear All";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // productError
            // 
            this.productError.AutoSize = true;
            this.productError.ForeColor = System.Drawing.Color.Red;
            this.productError.Location = new System.Drawing.Point(293, 46);
            this.productError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.productError.Name = "productError";
            this.productError.Size = new System.Drawing.Size(256, 25);
            this.productError.TabIndex = 9;
            this.productError.Text = "Please enter a valid product.";
            this.productError.Visible = false;
            // 
            // quantityError
            // 
            this.quantityError.AutoSize = true;
            this.quantityError.ForeColor = System.Drawing.Color.Red;
            this.quantityError.Location = new System.Drawing.Point(293, 138);
            this.quantityError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.quantityError.Name = "quantityError";
            this.quantityError.Size = new System.Drawing.Size(257, 25);
            this.quantityError.TabIndex = 8;
            this.quantityError.Text = "Please enter a valid number.";
            this.quantityError.Visible = false;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(267, 247);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 7;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged_1);
            // 
            // btn_RequestSubmit
            // 
            this.btn_RequestSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(224)))), ((int)(((byte)(222)))));
            this.btn_RequestSubmit.FlatAppearance.BorderSize = 0;
            this.btn_RequestSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RequestSubmit.Location = new System.Drawing.Point(193, 473);
            this.btn_RequestSubmit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_RequestSubmit.Name = "btn_RequestSubmit";
            this.btn_RequestSubmit.Size = new System.Drawing.Size(100, 34);
            this.btn_RequestSubmit.TabIndex = 6;
            this.btn_RequestSubmit.Text = "Submit";
            this.btn_RequestSubmit.UseVisualStyleBackColor = false;
            this.btn_RequestSubmit.Click += new System.EventHandler(this.btn_RequestSubmit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 288);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Quantity";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Product";
            // 
            // txtB_RequestDate
            // 
            this.txtB_RequestDate.Location = new System.Drawing.Point(35, 330);
            this.txtB_RequestDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_RequestDate.Name = "txtB_RequestDate";
            this.txtB_RequestDate.Size = new System.Drawing.Size(191, 30);
            this.txtB_RequestDate.TabIndex = 2;
            // 
            // txtB_RequestQuantity
            // 
            this.txtB_RequestQuantity.Location = new System.Drawing.Point(35, 178);
            this.txtB_RequestQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_RequestQuantity.Name = "txtB_RequestQuantity";
            this.txtB_RequestQuantity.Size = new System.Drawing.Size(533, 30);
            this.txtB_RequestQuantity.TabIndex = 1;
            // 
            // txtB_RequestProduct
            // 
            this.txtB_RequestProduct.Location = new System.Drawing.Point(35, 85);
            this.txtB_RequestProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtB_RequestProduct.Name = "txtB_RequestProduct";
            this.txtB_RequestProduct.Size = new System.Drawing.Size(533, 30);
            this.txtB_RequestProduct.TabIndex = 0;
            // 
            // page_ProMan
            // 
            this.page_ProMan.Controls.Add(this.block_ProductManager);
            this.page_ProMan.Controls.Add(this.txtB_Id);
            this.page_ProMan.Controls.Add(this.txtB_Description);
            this.page_ProMan.Controls.Add(this.txtB_Update);
            this.page_ProMan.Controls.Add(this.txtB_Price);
            this.page_ProMan.Controls.Add(this.txtB_Sold);
            this.page_ProMan.Controls.Add(this.txtB_Quanity);
            this.page_ProMan.Controls.Add(this.txtB_Name);
            this.page_ProMan.Controls.Add(this.comB_product);
            this.page_ProMan.Controls.Add(this.btn_Display);
            this.page_ProMan.Controls.Add(this.btn_RemoveProduct);
            this.page_ProMan.Controls.Add(this.btn_AddProduct);
            this.page_ProMan.Location = new System.Drawing.Point(4, 54);
            this.page_ProMan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_ProMan.Name = "page_ProMan";
            this.page_ProMan.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.page_ProMan.Size = new System.Drawing.Size(1024, 596);
            this.page_ProMan.TabIndex = 0;
            this.page_ProMan.Text = "Product Manager";
            this.page_ProMan.UseVisualStyleBackColor = true;
            // 
            // block_ProductManager
            // 
            this.block_ProductManager.Location = new System.Drawing.Point(8, 8);
            this.block_ProductManager.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.block_ProductManager.Name = "block_ProductManager";
            this.block_ProductManager.Size = new System.Drawing.Size(1008, 580);
            this.block_ProductManager.TabIndex = 25;
            // 
            // txtB_Id
            // 
            this.txtB_Id.BackColor = System.Drawing.Color.White;
            this.txtB_Id.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Id.Location = new System.Drawing.Point(211, 377);
            this.txtB_Id.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Id.Name = "txtB_Id";
            this.txtB_Id.ReadOnly = true;
            this.txtB_Id.Size = new System.Drawing.Size(603, 23);
            this.txtB_Id.TabIndex = 12;
            this.txtB_Id.Text = "Id:";
            // 
            // txtB_Description
            // 
            this.txtB_Description.BackColor = System.Drawing.Color.White;
            this.txtB_Description.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Description.Location = new System.Drawing.Point(211, 418);
            this.txtB_Description.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Description.Multiline = true;
            this.txtB_Description.Name = "txtB_Description";
            this.txtB_Description.ReadOnly = true;
            this.txtB_Description.Size = new System.Drawing.Size(603, 138);
            this.txtB_Description.TabIndex = 11;
            this.txtB_Description.Text = "Description:";
            // 
            // txtB_Update
            // 
            this.txtB_Update.BackColor = System.Drawing.Color.White;
            this.txtB_Update.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Update.Location = new System.Drawing.Point(211, 321);
            this.txtB_Update.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Update.Name = "txtB_Update";
            this.txtB_Update.ReadOnly = true;
            this.txtB_Update.Size = new System.Drawing.Size(603, 23);
            this.txtB_Update.TabIndex = 10;
            this.txtB_Update.Text = "Update Date:";
            // 
            // txtB_Price
            // 
            this.txtB_Price.BackColor = System.Drawing.Color.White;
            this.txtB_Price.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Price.Location = new System.Drawing.Point(211, 266);
            this.txtB_Price.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Price.Name = "txtB_Price";
            this.txtB_Price.ReadOnly = true;
            this.txtB_Price.Size = new System.Drawing.Size(603, 23);
            this.txtB_Price.TabIndex = 9;
            this.txtB_Price.Text = "Price:";
            // 
            // txtB_Sold
            // 
            this.txtB_Sold.BackColor = System.Drawing.Color.White;
            this.txtB_Sold.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Sold.Location = new System.Drawing.Point(211, 210);
            this.txtB_Sold.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Sold.Name = "txtB_Sold";
            this.txtB_Sold.ReadOnly = true;
            this.txtB_Sold.Size = new System.Drawing.Size(603, 23);
            this.txtB_Sold.TabIndex = 8;
            this.txtB_Sold.Text = "Sold:";
            // 
            // txtB_Quanity
            // 
            this.txtB_Quanity.BackColor = System.Drawing.Color.White;
            this.txtB_Quanity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Quanity.Location = new System.Drawing.Point(211, 156);
            this.txtB_Quanity.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Quanity.Name = "txtB_Quanity";
            this.txtB_Quanity.ReadOnly = true;
            this.txtB_Quanity.Size = new System.Drawing.Size(603, 23);
            this.txtB_Quanity.TabIndex = 7;
            this.txtB_Quanity.Text = "Quanity:";
            // 
            // txtB_Name
            // 
            this.txtB_Name.BackColor = System.Drawing.Color.White;
            this.txtB_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtB_Name.Location = new System.Drawing.Point(211, 101);
            this.txtB_Name.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.txtB_Name.Name = "txtB_Name";
            this.txtB_Name.ReadOnly = true;
            this.txtB_Name.Size = new System.Drawing.Size(603, 23);
            this.txtB_Name.TabIndex = 6;
            this.txtB_Name.Text = "Name:";
            // 
            // comB_product
            // 
            this.comB_product.FormattingEnabled = true;
            this.comB_product.Location = new System.Drawing.Point(211, 36);
            this.comB_product.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comB_product.Name = "comB_product";
            this.comB_product.Size = new System.Drawing.Size(601, 33);
            this.comB_product.TabIndex = 5;
            // 
            // btn_Display
            // 
            this.btn_Display.Location = new System.Drawing.Point(820, 36);
            this.btn_Display.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Display.Name = "btn_Display";
            this.btn_Display.Size = new System.Drawing.Size(120, 33);
            this.btn_Display.TabIndex = 4;
            this.btn_Display.Text = "Display";
            this.btn_Display.UseVisualStyleBackColor = true;
            this.btn_Display.Click += new System.EventHandler(this.btn_Display_Click);
            // 
            // btn_RemoveProduct
            // 
            this.btn_RemoveProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_RemoveProduct.FlatAppearance.BorderSize = 0;
            this.btn_RemoveProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RemoveProduct.Location = new System.Drawing.Point(820, 523);
            this.btn_RemoveProduct.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_RemoveProduct.Name = "btn_RemoveProduct";
            this.btn_RemoveProduct.Size = new System.Drawing.Size(120, 34);
            this.btn_RemoveProduct.TabIndex = 3;
            this.btn_RemoveProduct.Text = "Remove";
            this.btn_RemoveProduct.UseVisualStyleBackColor = false;
            this.btn_RemoveProduct.Click += new System.EventHandler(this.btn_RemoveProduct_Click);
            // 
            // btn_AddProduct
            // 
            this.btn_AddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(224)))), ((int)(((byte)(222)))));
            this.btn_AddProduct.FlatAppearance.BorderSize = 0;
            this.btn_AddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AddProduct.Location = new System.Drawing.Point(820, 469);
            this.btn_AddProduct.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.btn_AddProduct.Name = "btn_AddProduct";
            this.btn_AddProduct.Size = new System.Drawing.Size(120, 34);
            this.btn_AddProduct.TabIndex = 2;
            this.btn_AddProduct.Text = "Add";
            this.btn_AddProduct.UseVisualStyleBackColor = false;
            this.btn_AddProduct.Click += new System.EventHandler(this.btn_AddProduct_Click);
            // 
            // tabI
            // 
            this.tabI.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.tabI.AllowDrop = true;
            this.tabI.Controls.Add(this.page_ProMan);
            this.tabI.Controls.Add(this.page_OrdReq);
            this.tabI.Controls.Add(this.page_UsePri);
            this.tabI.Controls.Add(this.page_StoUpd);
            this.tabI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabI.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabI.ItemSize = new System.Drawing.Size(250, 50);
            this.tabI.Location = new System.Drawing.Point(0, 0);
            this.tabI.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.tabI.Multiline = true;
            this.tabI.Name = "tabI";
            this.tabI.Padding = new System.Drawing.Point(40, 6);
            this.tabI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabI.SelectedIndex = 0;
            this.tabI.Size = new System.Drawing.Size(1032, 654);
            this.tabI.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabI.TabIndex = 1;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // staffTableAdapter
            // 
            this.staffTableAdapter.ClearBeforeFill = true;
            // 
            // StaffView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 654);
            this.Controls.Add(this.tabI);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "StaffView";
            this.Text = "Aglets 4 Dayz";
            this.Load += new System.EventHandler(this.StaffView_Load);
            this.page_StoUpd.ResumeLayout(false);
            this.page_StoUpd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.products)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.staffBaseDataSet2)).EndInit();
            this.page_UsePri.ResumeLayout(false);
            this.page_UsePri.PerformLayout();
            this.page_OrdReq.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.page_ProMan.ResumeLayout(false);
            this.page_ProMan.PerformLayout();
            this.tabI.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage page_StoUpd;
        private System.Windows.Forms.TabPage page_UsePri;
        private System.Windows.Forms.Button btn_EditGroups;
        private System.Windows.Forms.Button btn_RemoveUser;
        private System.Windows.Forms.Button btn_Edit;
        private System.Windows.Forms.RichTextBox rText_UserInformation;
        private System.Windows.Forms.TabPage page_OrdReq;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label productError;
        private System.Windows.Forms.Label quantityError;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button btn_RequestSubmit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtB_RequestDate;
        private System.Windows.Forms.TextBox txtB_RequestQuantity;
        private System.Windows.Forms.TextBox txtB_RequestProduct;
        private System.Windows.Forms.TabPage page_ProMan;
        private System.Windows.Forms.Button btn_Display;
        private System.Windows.Forms.Button btn_RemoveProduct;
        private System.Windows.Forms.Button btn_AddProduct;
        internal System.Windows.Forms.TabControl tabI;
        private System.Windows.Forms.ComboBox comB_product;
        private System.Windows.Forms.TextBox txtB_Id;
        private System.Windows.Forms.TextBox txtB_Description;
        private System.Windows.Forms.TextBox txtB_Update;
        private System.Windows.Forms.TextBox txtB_Price;
        private System.Windows.Forms.TextBox txtB_Sold;
        private System.Windows.Forms.TextBox txtB_Quanity;
        private System.Windows.Forms.TextBox txtB_Name;
        private System.Windows.Forms.TextBox txtB_Notes;
        private System.Windows.Forms.TextBox txtB_Phone;
        private System.Windows.Forms.TextBox txtB_Position;
        private System.Windows.Forms.TextBox txtB_StaffName;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_DisplayUser;
        private Products products;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private ProductsTableAdapters.ProductsTableAdapter productsTableAdapter;
        private StaffBaseDataSet2 staffBaseDataSet2;
        private System.Windows.Forms.BindingSource staffBindingSource;
        private StaffBaseDataSet2TableAdapters.StaffTableAdapter staffTableAdapter;
        private System.Windows.Forms.Panel block_UserPrivileges;
        private System.Windows.Forms.Panel block_StockUpdate;
        private System.Windows.Forms.Panel block_OrderRequest;
        private System.Windows.Forms.TextBox txtB_AddStock;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_AddStock;
        private System.Windows.Forms.TextBox txtB_UpdateDate;
        private System.Windows.Forms.TextBox txtB_ProductPrice;
        private System.Windows.Forms.TextBox txtB_ProductQuantity;
        private System.Windows.Forms.TextBox txtB_ProductName;
        private System.Windows.Forms.ComboBox comB_AddStock;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel block_ProductManager;
    }
}

