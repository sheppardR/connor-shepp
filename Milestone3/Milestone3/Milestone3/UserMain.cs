﻿// This is our original work - Connor and Sheppard

using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using EditGroups;
using EditUser;


namespace Milestone3
{
    public partial class StaffView : Form
    {
        private const string Connect =
            @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        //private const string Connect =
        // @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\Connor-Shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        private string selected = "";
        private static string _username = "";
        private string ProductOrdered;
        private double quantity;
        private string DateOrdered;
        private Boolean Duplicate = false;

        public StaffView()
        {
            InitializeComponent();
        }

        public StaffView(string username)
        {
            InitializeComponent();
            _username = username;
            if (getUseCases("EditUser"))
                block_UserPrivileges.Hide();
            else block_UserPrivileges.Show();
            if (getUseCases("EditProduct"))
                block_ProductManager.Hide();
            else block_ProductManager.Show();
            if (getUseCases("UpdateStock"))
                block_StockUpdate.Hide();
            else block_StockUpdate.Show();
            if (getUseCases("ManageOrders"))
                block_OrderRequest.Hide();
            else block_OrderRequest.Show();
            refreshBox();
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            var f = new UserPrivileges();
            f.ShowDialog();
            refreshBox();
        }

        private void btn_AddProduct_Click(object sender, EventArgs e)
        {
            var f = new ItemEditor.ItemEditor();
            f.ShowDialog();
            StaffView_Load(sender, new EventArgs());
            this.productsTableAdapter.Fill(this.products._Products);
            refreshBox();
        }

        private void btn_EditGroups_Click(object sender, EventArgs e)
        {
            var f = new EditGroupDialogue();
            f.ShowDialog();
            refreshBox();
        }


        //order page


        private void btn_RequestSubmit_Click(object sender, EventArgs e)
        {
            ProductOrdered = txtB_RequestProduct.Text;
            DateOrdered = txtB_RequestDate.Text;

            productError.Hide();

            if (Double.TryParse(txtB_RequestQuantity.Text, out quantity))
            {
                quantityError.Hide();
            }
            else quantityError.Show();


            if (txtB_RequestProduct.Equals(""))
            {
                productError.Show();
            }

            else
            {
                AddOrder(ProductOrdered, quantity, DateOrdered);
                txtB_RequestProduct.Text = txtB_RequestQuantity.Text = txtB_RequestDate.Text = "";
            }
            refreshBox();
        }

        private void AddOrder(String porder, double q, string date)
        {
            porder = ProductOrdered;
            q = quantity;
            date = DateOrdered;

            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"INSERT INTO Orders (OrderDate, ProductOrdered, QuantityOrdered) 
                    VALUES ('" + date + @"', '" + porder + @"', '" + q + @"');";
                    var comm = new SqlCommand(query, conn);
                    //execute query
                    conn.Open();
                    comm.ExecuteReader();

                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ERROR");
                }
            }
            refreshBox();
        }

        private void monthCalendar1_DateChanged_1(object sender, DateRangeEventArgs e)
        {
            txtB_RequestDate.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtB_RequestDate.Text = txtB_RequestQuantity.Text = txtB_RequestProduct.Text = "";
            productError.Hide();
            quantityError.Hide();
            refreshBox();
        }


        private void StaffView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'staffBaseDataSet2.Staff' table. You can move, or remove it, as needed.
            this.staffTableAdapter.Fill(this.staffBaseDataSet2.Staff);
            // TODO: This line of code loads data into the 'products._Products' table. You can move, or remove it, as needed.
            this.productsTableAdapter.Fill(this.products._Products);
            // TODO: This line of code loads data into the 'agletBaseDataSet2.Staff' table. You can move, or remove it, as needed.

            // TODO: This line of code loads data into the 'staffBaseDataSet2.Staff' table. You can move, or remove it, as needed.
            this.staffTableAdapter.Fill(this.staffBaseDataSet2.Staff);
            // TODO: This line of code loads data into the 'products._Products' table. You can move, or remove it, as needed

        }


        private void btn_Display_Click(object sender, EventArgs e)
        {
            string result = "";
            var product = comB_product.Text;
            product = removeSpace(product);
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"SELECT * FROM Products WHERE ProductName = '" + product + @"'";
                    var command = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    var q = command.ExecuteReader();
                    while (q.Read())
                    {
                        txtB_Name.Text = @"Name: " + q["ProductName"];
                        txtB_Quanity.Text = @"Quantity: " + q["ProductQuantity"];
                        txtB_Price.Text = @"Price: $" + q["ProductPrice"];
                        txtB_Sold.Text = @"Sold: " + q["ProductSold"];
                        txtB_Update.Text = @"Update Date: " + q["UpdateDate"];
                        txtB_Description.Text = @"Description: " + q["ProductDescription"];
                        txtB_Id.Text = @"Id: " + q["Id"];
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR - connection in UserMain");
                }
            }
            refreshBox();
        }

        private String removeSpace(string str)
        {
            var result = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str.Substring(i, 1).Equals(" ") && str.Substring(i + 1, 1).Equals(" "))
                    return result;
                else result += str.Substring(i, 1);
            }
            return str;
        }

        private void btn_DisplayUser_Click(object sender, EventArgs e)
        {

            var staff = comboBox1.Text;
            staff = removeSpace(staff);
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"SELECT * FROM Staff WHERE StaffName = '" + staff + @"'";
                    var command = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    var q = command.ExecuteReader();
                    while (q.Read())
                    {
                        txtB_StaffName.Text = @"Name: " + q["StaffName"];
                        txtB_Position.Text = @"Position: " + q["Position"];
                        txtB_Phone.Text = @"Phone Number: " + q["PhoneNumber"];
                        txtB_Notes.Text = @"Notes: " + q["Notes"];

                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR my dude");
                }
            }
            refreshBox();
        }

        public static bool getUseCases(string formName)
        {
            string pos = "";
            string result = "False";
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"SELECT Position FROM Staff WHERE Username = '" + _username + @"'";
                    var cmd = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    var qr = cmd.ExecuteReader();
                    while (qr.Read())
                    {
                        pos = removeSpaces(qr["Position"] + "");
                    }
                    conn.Close();
                    var query2 = @"SELECT " + formName + " FROM Groups WHERE GroupName = '" + pos + @"'";
                    cmd = new SqlCommand(query2, conn) { CommandText = query2 };
                    conn.Open();
                    qr = cmd.ExecuteReader();
                    while (qr.Read())
                    {
                        result = qr[formName] + "";
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR - getUseCases got a monkey wrench");
                }
            }
            return result.ToUpper().Equals("TRUE");
        }

        public static string removeSpaces(string str)
        {
            string r = "";
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 1).Equals(" ") && i != str.Length - 1 && str.Substring(i + 1, 1).Equals(" "))
                    return r;
                r += str.Substring(i, 1);
            }
            return r;
        }

        private string NametoAdd;

        private void AddStock(String name)
        {
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"UPDATE Products SET ProductQuantity += '" + StockTxtB + @"' WHERE ProductName = '" +
                                name + "'";
                    var command = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    command.ExecuteReader();

                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR there's a snake in my boot...");
                }
            }

        }

        private int StockTxtB;

        private void btn_AddStock_Click(object sender, EventArgs e)
        {
            int temp = 0;
            if (Int32.TryParse(txtB_AddStock.Text, out temp))
            {
                StockTxtB = int.Parse(txtB_AddStock.Text);
                NametoAdd = comB_AddStock.Text;
                NametoAdd = removeSpaces(NametoAdd);
                AddStock(NametoAdd);
            }
            else
            {
                MessageBox.Show(@"Invalid entry");
            }
            refreshBox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var product = comB_AddStock.Text;
            product = removeSpace(product);
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"SELECT * FROM Products WHERE ProductName = '" + product + @"'";
                    var command = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    var q = command.ExecuteReader();
                    while (q.Read())
                    {
                        txtB_ProductName.Text = "Name: " + q["ProductName"];
                        txtB_ProductQuantity.Text = "Quantity: " + q["ProductQuantity"];
                        txtB_ProductPrice.Text = "Price: " + q["ProductPrice"];
                        txtB_UpdateDate.Text = "Update Date: " + q["UpdateDate"];

                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR someone's poisoned the waterhole");
                }
            }
            refreshBox();
        }

        private void btn_RemoveProduct_Click(object sender, EventArgs e)
        {
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    var query = @"DELETE Products WHERE ProductName = '" + removeSpaces(comB_product.Text) + @"'";
                    var cmd = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    cmd.ExecuteReader();
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"ERROR - RemoveProduct? Yeah, good luck.");
                }
                this.productsTableAdapter.Fill(this.products._Products);
                comB_product.Refresh();
            }
        }

        public void refreshBox()
        {
            comB_product.Items.Clear();
            comB_product.Text = "";
            comboBox1.Items.Clear();
            comboBox1.Text = "";
            comB_AddStock.Items.Clear();
            comB_AddStock.Text = "";
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    //write a query
                    string query = "SELECT ProductName FROM Products";
                    var comm = new SqlCommand(query, conn) { CommandText = query };
                    
                    // Update Product Tables
                    conn.Open();
                    var dr = comm.ExecuteReader();
                    //execute query
                    while (dr.Read())
                    {
                        //create display string
                        string val = dr["ProductName"] + "";
                        //get stuff back
                        comB_product.Items.Add(val);
                        comB_AddStock.Items.Add(val);
                    }
                    conn.Close();
                    // Update Staff Tables
                    query = "SELECT StaffName FROM Staff";
                    comm = new SqlCommand(query, conn) { CommandText = query };
                    conn.Open();
                    dr = comm.ExecuteReader();
                    while (dr.Read())
                    {
                        string val = dr["StaffName"] + "";
                        comboBox1.Items.Add(val);
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"If this shows up during our presentation, my life is over.");
                }
            }
        }
    }
}

/*
 * READ FIRST
 * This explains how to get the tabs to align to the left side:
 * http://stackoverflow.com/questions/7498413/vertical-tab-control-with-horizontal-text-in-winforms
 * 
 * -----Naming things-----
 * Buttons: btn_ButtonName
 * Text Boxes (input): txtB_BoxName
 *            (read-only): text_BoxName
 *            (rich-text): rText_BoxName
 * Tab-pages: page_PagNam (first three letters of first two words)
 * Forms: FormName
 * 
 * 
 * -----Change Log-----
 * Connor - 11/7 -> made form for Staff Login main.
 * 
 * Sheppard 12/10 -> done stuff
 */
