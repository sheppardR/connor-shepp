﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfirmForm;

namespace EditUser
{
    public partial class UserPrivileges : Form
    {
        private const string Connect =
           @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        //private const string Connect = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        private string _input = null;

        public UserPrivileges()
        {
            InitializeComponent();
        }

        public UserPrivileges(string edit)
        {
            InitializeComponent();
            _input = edit;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void img_Product_Click(object sender, EventArgs e)
        {

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            bool name = false;
            bool position = false;
            bool list = false;
            bool phone = false;
            if (txtB_UserPhone.Text.Length < 10)
            {
                errorMessage_Phone.Show();
            }
            else
            {
                errorMessage_Phone.Hide();
                phone = true;
            }
            if (txtB_UserName.Text.Equals(String.Empty))
            {
                errorMesssage_NameEmpty.Show();
                errorMessage_NameExists.Hide();
            }
            else if (txtB_UserName.Text.Equals("A name that exists"))
            {
                errorMessage_NameExists.Show();
                errorMesssage_NameEmpty.Hide();
            }
            else
            {
                name = true;
                errorMessage_NameExists.Hide();
                errorMesssage_NameEmpty.Hide();
            }
            if (txtB_UserPassword.Text.Equals(String.Empty))
            {
                errorMessage_PositionEmpty.Show();
                errorMessage_PositionNotExist.Hide();
            }
            else if (txtB_UserPassword.Text.Equals("A position that does not exist"))
            {
                errorMessage_PositionEmpty.Hide();
                errorMessage_PositionNotExist.Show();
            }
            else
            {
                position = true;
                errorMessage_PositionEmpty.Hide();
                errorMessage_PositionNotExist.Hide();
            }
            if (cList_Privileges.CheckedItems.Count != 0)
            {
                list = true;
                errorMessage_Privileges.Hide();
            }
            else errorMessage_Privileges.Show();
            if (!(list && name && position && phone))
                return;
            var f = new Confirm();
            f.ShowDialog();
            if (Confirm.YesPressed)
            {
                if (_input == null)
                    addToDB(txtB_UserName.Text, txtB_UserPassword.Text, txtB_UserPhone.Text, cList_Privileges.Text, txtB_UserNotes.Text);
                else updateDB(txtB_UserName.Text, txtB_UserPassword.Text, txtB_UserPhone.Text, cList_Privileges.Text, txtB_UserNotes.Text);
                this.Hide();
            }
        }

        private void addToDB(string name, string password, string phoneNumber, string privileges, string notes)
        {
            var username = name.Substring(0, 4) + phoneNumber.Substring(phoneNumber.Length - 5);
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    // Write query
                    var query = @"INSERT INTO Staff (StaffName, Username, Password, PhoneNumber, Position, Notes) " +
@"VALUES ('" + name + @"', '" + username + @"', '" + password + @"', '" + phoneNumber + @"', '" + privileges + @"', '" + notes + @"');";
                    var comm = new SqlCommand(query, conn);
                    conn.Open();
                    comm.ExecuteReader();

                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"Error in Edit User");
                }
            }
        } // Connor - Wednesday

        private void updateDB(string name, string password, string phoneNumber, string privileges, string notes)
        {
            using (var conn = new SqlConnection())
            {
                try
                {
                    //todo PROBLEM - NEED WAY TO KNOW WHO IS BEING EDITED.
                    var query = @"UPDATE Staff SET StaffName = '" + name + @"', Password = '" + password + @"', phoneNumber = '" + phoneNumber +
                        @"', Position = '" + privileges + @"', Notes = '" + notes + @"', WHERE StaffName = '" + _input + @"';";
                    var comm = new SqlCommand(query, conn);
                    conn.Open();
                    comm.ExecuteReader();
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show(@"Error");
                }
            }
        } // Connor - Friday

        private string name;

        private void UserPrivileges_Load(object sender, EventArgs e)
        {

        }
    }
}
