﻿namespace EditUser
{
    partial class UserPrivileges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtB_UserName = new System.Windows.Forms.TextBox();
            this.txtB_UserPassword = new System.Windows.Forms.TextBox();
            this.txtB_UserPhone = new System.Windows.Forms.TextBox();
            this.txtB_UserNotes = new System.Windows.Forms.TextBox();
            this.cList_Privileges = new System.Windows.Forms.CheckedListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.errorMessage_NameExists = new System.Windows.Forms.Label();
            this.errorMesssage_NameEmpty = new System.Windows.Forms.Label();
            this.errorMessage_PositionNotExist = new System.Windows.Forms.Label();
            this.errorMessage_PositionEmpty = new System.Windows.Forms.Label();
            this.errorMessage_Privileges = new System.Windows.Forms.Label();
            this.errorMessage_Phone = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 89);
            this.label2.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 156);
            this.label3.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Phone Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(390, 22);
            this.label4.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Notes";
            // 
            // txtB_UserName
            // 
            this.txtB_UserName.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_UserName.Location = new System.Drawing.Point(26, 52);
            this.txtB_UserName.Margin = new System.Windows.Forms.Padding(6, 5, 3, 5);
            this.txtB_UserName.Name = "txtB_UserName";
            this.txtB_UserName.Size = new System.Drawing.Size(324, 25);
            this.txtB_UserName.TabIndex = 4;
            // 
            // txtB_UserPassword
            // 
            this.txtB_UserPassword.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_UserPassword.Location = new System.Drawing.Point(26, 119);
            this.txtB_UserPassword.Margin = new System.Windows.Forms.Padding(6, 5, 3, 5);
            this.txtB_UserPassword.Name = "txtB_UserPassword";
            this.txtB_UserPassword.Size = new System.Drawing.Size(324, 25);
            this.txtB_UserPassword.TabIndex = 5;
            // 
            // txtB_UserPhone
            // 
            this.txtB_UserPhone.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_UserPhone.Location = new System.Drawing.Point(26, 186);
            this.txtB_UserPhone.Margin = new System.Windows.Forms.Padding(6, 5, 3, 5);
            this.txtB_UserPhone.Name = "txtB_UserPhone";
            this.txtB_UserPhone.Size = new System.Drawing.Size(324, 25);
            this.txtB_UserPhone.TabIndex = 6;
            // 
            // txtB_UserNotes
            // 
            this.txtB_UserNotes.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtB_UserNotes.Location = new System.Drawing.Point(394, 52);
            this.txtB_UserNotes.Margin = new System.Windows.Forms.Padding(6, 5, 3, 5);
            this.txtB_UserNotes.Multiline = true;
            this.txtB_UserNotes.Name = "txtB_UserNotes";
            this.txtB_UserNotes.Size = new System.Drawing.Size(324, 161);
            this.txtB_UserNotes.TabIndex = 7;
            // 
            // cList_Privileges
            // 
            this.cList_Privileges.AccessibleRole = System.Windows.Forms.AccessibleRole.RadioButton;
            this.cList_Privileges.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cList_Privileges.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cList_Privileges.Items.AddRange(new object[] {
            "Admin",
            "Clerk",
            "General Manager",
            "Inventory Manager"});
            this.cList_Privileges.Location = new System.Drawing.Point(26, 251);
            this.cList_Privileges.Name = "cList_Privileges";
            this.cList_Privileges.Size = new System.Drawing.Size(535, 40);
            this.cList_Privileges.Sorted = true;
            this.cList_Privileges.TabIndex = 9;
            this.cList_Privileges.UseCompatibleTextRendering = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Privileges";
            // 
            // btn_Save
            // 
            this.btn_Save.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.Location = new System.Drawing.Point(598, 304);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(120, 35);
            this.btn_Save.TabIndex = 11;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cancel.Location = new System.Drawing.Point(598, 251);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(120, 35);
            this.btn_Cancel.TabIndex = 12;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            // 
            // errorMessage_NameExists
            // 
            this.errorMessage_NameExists.AutoSize = true;
            this.errorMessage_NameExists.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_NameExists.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_NameExists.Location = new System.Drawing.Point(239, 22);
            this.errorMessage_NameExists.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_NameExists.Name = "errorMessage_NameExists";
            this.errorMessage_NameExists.Size = new System.Drawing.Size(88, 20);
            this.errorMessage_NameExists.TabIndex = 15;
            this.errorMessage_NameExists.Text = "Name exists";
            this.errorMessage_NameExists.Visible = false;
            // 
            // errorMesssage_NameEmpty
            // 
            this.errorMesssage_NameEmpty.AutoSize = true;
            this.errorMesssage_NameEmpty.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMesssage_NameEmpty.ForeColor = System.Drawing.Color.Red;
            this.errorMesssage_NameEmpty.Location = new System.Drawing.Point(132, 22);
            this.errorMesssage_NameEmpty.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMesssage_NameEmpty.Name = "errorMesssage_NameEmpty";
            this.errorMesssage_NameEmpty.Size = new System.Drawing.Size(170, 20);
            this.errorMesssage_NameEmpty.TabIndex = 16;
            this.errorMesssage_NameEmpty.Text = "Name must not be empty";
            this.errorMesssage_NameEmpty.Visible = false;
            // 
            // errorMessage_PositionNotExist
            // 
            this.errorMessage_PositionNotExist.AutoSize = true;
            this.errorMessage_PositionNotExist.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_PositionNotExist.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_PositionNotExist.Location = new System.Drawing.Point(156, 89);
            this.errorMessage_PositionNotExist.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_PositionNotExist.Name = "errorMessage_PositionNotExist";
            this.errorMessage_PositionNotExist.Size = new System.Drawing.Size(157, 20);
            this.errorMessage_PositionNotExist.TabIndex = 17;
            this.errorMessage_PositionNotExist.Text = "Position does not exist";
            this.errorMessage_PositionNotExist.Visible = false;
            // 
            // errorMessage_PositionEmpty
            // 
            this.errorMessage_PositionEmpty.AutoSize = true;
            this.errorMessage_PositionEmpty.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_PositionEmpty.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_PositionEmpty.Location = new System.Drawing.Point(116, 89);
            this.errorMessage_PositionEmpty.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_PositionEmpty.Name = "errorMessage_PositionEmpty";
            this.errorMessage_PositionEmpty.Size = new System.Drawing.Size(195, 20);
            this.errorMessage_PositionEmpty.TabIndex = 18;
            this.errorMessage_PositionEmpty.Text = "Password must not be empty";
            this.errorMessage_PositionEmpty.Visible = false;
            // 
            // errorMessage_Privileges
            // 
            this.errorMessage_Privileges.AutoSize = true;
            this.errorMessage_Privileges.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_Privileges.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_Privileges.Location = new System.Drawing.Point(370, 223);
            this.errorMessage_Privileges.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_Privileges.Name = "errorMessage_Privileges";
            this.errorMessage_Privileges.Size = new System.Drawing.Size(154, 20);
            this.errorMessage_Privileges.TabIndex = 19;
            this.errorMessage_Privileges.Text = "Must assign privileges";
            this.errorMessage_Privileges.Visible = false;
            // 
            // errorMessage_Phone
            // 
            this.errorMessage_Phone.AutoSize = true;
            this.errorMessage_Phone.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage_Phone.ForeColor = System.Drawing.Color.Red;
            this.errorMessage_Phone.Location = new System.Drawing.Point(219, 156);
            this.errorMessage_Phone.Margin = new System.Windows.Forms.Padding(15, 5, 3, 5);
            this.errorMessage_Phone.Name = "errorMessage_Phone";
            this.errorMessage_Phone.Size = new System.Drawing.Size(105, 20);
            this.errorMessage_Phone.TabIndex = 20;
            this.errorMessage_Phone.Text = "Invalid number";
            this.errorMessage_Phone.Visible = false;
            // 
            // UserPrivileges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 361);
            this.Controls.Add(this.errorMessage_Phone);
            this.Controls.Add(this.errorMessage_Privileges);
            this.Controls.Add(this.errorMessage_PositionEmpty);
            this.Controls.Add(this.errorMessage_PositionNotExist);
            this.Controls.Add(this.errorMesssage_NameEmpty);
            this.Controls.Add(this.errorMessage_NameExists);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cList_Privileges);
            this.Controls.Add(this.txtB_UserNotes);
            this.Controls.Add(this.txtB_UserPhone);
            this.Controls.Add(this.txtB_UserPassword);
            this.Controls.Add(this.txtB_UserName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserPrivileges";
            this.Text = "Edit User";
            this.Load += new System.EventHandler(this.UserPrivileges_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtB_UserName;
        private System.Windows.Forms.TextBox txtB_UserPassword;
        private System.Windows.Forms.TextBox txtB_UserPhone;
        private System.Windows.Forms.TextBox txtB_UserNotes;
        private System.Windows.Forms.CheckedListBox cList_Privileges;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label errorMessage_NameExists;
        private System.Windows.Forms.Label errorMesssage_NameEmpty;
        private System.Windows.Forms.Label errorMessage_PositionNotExist;
        private System.Windows.Forms.Label errorMessage_PositionEmpty;
        private System.Windows.Forms.Label errorMessage_Privileges;
        private System.Windows.Forms.Label errorMessage_Phone;
    }
}

