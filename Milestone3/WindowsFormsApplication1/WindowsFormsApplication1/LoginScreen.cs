﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Milestone3;
using Browser;

namespace LoginScreen
{
    public partial class Form1 : Form
    {
        public const string Connect = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\speed\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        //private const string Connect = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Administrator\Documents\connor-shepp\Milestone3\Milestone3\Milestone3\AgletBase.mdf;Integrated Security=True";

        public Form1()
        {
            InitializeComponent();
        }

        private void toBrowser_Click(object sender, System.EventArgs e)
        {
            var f = new BrowserPage();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }

        private void loginButton_Click(object sender, System.EventArgs e)
        {
            string usernameInput = textBox1.Text;
            string passwordInput = textBox2.Text;
          
            if (validate(usernameInput, passwordInput) == 1)
            {
                label5.Text = "Username and Password must\n not be blank.";
                label5.Show();
            }
            else if (validate(usernameInput, passwordInput) == 2)
            {
                label5.Text = "Invalid Username or Password.";
                label5.Show();
            }
            else if (validate(usernameInput, passwordInput) == 3)
            {
                label5.Hide();
                var f = new StaffView(textBox1.Text);
                textBox2.Text = string.Empty;
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private static int validate(string u, string p)
        {
            var caseResult = 0;
            string usernameInput = u;
            string passwordInput = p;
            string username = "";
            string password = "";
            using (var conn = new SqlConnection(Connect))
            {
                try
                {
                    // generate query
                    var Query =
                        @"SELECT Password, Username FROM Staff WHERE Username = '" + usernameInput + @"'";
                    var comm = new SqlCommand(Query, conn);
                    // execute query
                    conn.Open();
                    var qr = comm.ExecuteReader();
                    while (qr.Read())
                    {
                        password = qr["Password"] + "";
                        username = qr["Username"] + "";
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("ERROR - Connection");
                }
            }
            if (usernameInput.Length < 1 || passwordInput.Length < 1)
                caseResult = 1;
            else if (usernameInput.Equals(username) && !passwordInput.Equals(password))
                caseResult = 2;
            else if (usernameInput.Equals(username) && passwordInput.Equals(password) && password != null)
                caseResult = 3;
            return caseResult;
        }
    }
}
